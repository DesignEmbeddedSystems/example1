/* config.h */ 

/* ports */
#define RIGHT_MOTOR NXT_PORT_A
#define LEFT_MOTOR NXT_PORT_B
#define LAMP NXT_PORT_C

#define LIGHT  NXT_PORT_S1
#define SONIC  NXT_PORT_S2
#define TOUCH  NXT_PORT_S3

/* states */
#define INIT 0
#define NONE 1
#define LIGHT_RULE 2
#define TOUCH_RULE 3
#define SONIC_RULE 4

/* time outs */
#define LIGHT_TURN_TIME 500U
#define TOUCH_BACK_TIME 750U
#define SONIC_TURN_TIME 500U

/* threshold */
#define LIGHT_THRESHOLD 460
#define DISTANCE_THRESHOLD 35

