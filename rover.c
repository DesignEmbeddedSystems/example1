/* rover.c */ 
#include "kernel.h"
#include "kernel_id.h"
#include "ecrobot_interface.h"

#include "string.h"
/* own definitions */
#include "config.h"

/* OSEK declarations */
DeclareCounter(SysTimerCnt);
DeclareTask(Task1);
DeclareTask(LCDMonitor);

/* LEJOS OSEK hooks */
void ecrobot_device_initialize()
{
  ecrobot_set_light_sensor_active(LIGHT);
  /* ecrobot_init_sonar_sensor(SONIC);   */
  nxt_motor_set_speed(RIGHT_MOTOR, 0, 1); 
  nxt_motor_set_speed(LEFT_MOTOR, 0, 1);
  /* nxt_motor_set_speed(LAMP, 0, 1);   */
}

void ecrobot_device_terminate()
{
  ecrobot_set_light_sensor_inactive(LIGHT);
  /* ecrobot_term_sonar_sensor(SONIC); */
  nxt_motor_set_speed(RIGHT_MOTOR, 0, 1); 
  nxt_motor_set_speed(LEFT_MOTOR, 0, 1); 
  /* nxt_motor_set_speed(LAMP, 0, 1);  */
}

/* LEJOS OSEK hook to be invoked from an ISR in category 2 */
void user_1ms_isr_type2(void)
{
  StatusType ercd;

  ercd = SignalCounter(SysTimerCnt); /* Increment OSEK Alarm Counter */ 
  if(ercd != E_OK)
  {
    ShutdownOS(ercd);
  }
}

/* global variables  */
CHAR status[8];
U32 starttime = 0;

/*============================================================================
 * Task: Task1
 */

/* Task1 - executed periodically */
/* THIS IS A RANDOM EXAMPLE - MODIFY - ALSO CONSTANTS IN config.h */
TASK(Task1)
{ 
  
  if (ecrobot_get_light_sensor(LIGHT) < LIGHT_THRESHOLD){
    starttime = ecrobot_get_systick_ms();
    /* nxt_motor_set_speed(LAMP, 100, 1);  */
    /* drive back  */
    nxt_motor_set_speed(RIGHT_MOTOR, -50, 1);
    nxt_motor_set_speed(LEFT_MOTOR, -50, 1);
    strcpy(status,"light");
  }
  else if (ecrobot_get_systick_ms() - starttime > LIGHT_TURN_TIME) {
    /* drive forward */
    nxt_motor_set_speed(RIGHT_MOTOR, 50, 1);
    nxt_motor_set_speed(LEFT_MOTOR, 50, 1); 
    strcpy(status,"none"); 
   }
   
   /* next maybe later: */
    /* (ecrobot_get_touch_sensor(TOUCH)==1) */
    /* (ecrobot_get_sonar_sensor(SONIC) < DISTANCE_THRESHOLD) */

TerminateTask();
}

/*============================================================================
 * Task: LCDMonitor
 */
TASK(LCDMonitor)
{
  ecrobot_status_monitor(status);

  TerminateTask();
}

/******************************** END OF FILE ********************************/

